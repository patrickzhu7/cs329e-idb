import os
import psycopg2
import unittest
from models import db, app, Book, Author, Publisher

#conn = psycopg2.connect(host="localhost", database="bookdb", user="patrick", password="sqlRAGTAG", port="5432")
#conn = psycopg2.connect(host="localhost", database="postgres", user="postgres", password="123", port="5432")

#cursor = conn.cursor()

class DBTestCases(unittest.TestCase):
    def test_insert_book_1(self):
        s = Book(id='5001', title = 'booktest1')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Book).filter_by(id='5001').first()
        self.assertEqual(str(s2.id), '5001')

        s2.title = "booktesting1"
        db.session.commit()

        s3 = db.session.query(Book).filter_by(id='5001').first()
        self.assertEqual(str(s3.title), "booktesting1")

        db.session.delete(s3)
        db.session.commit()
    
    def test_insert_book_2(self):
        s = Book(id='5002', title = 'booktest2')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Book).filter_by(id='5002').first()
        self.assertEqual(str(s2.id), '5002')

        s2.title = "booktesting2"
        db.session.commit()

        s3 = db.session.query(Book).filter_by(id='5002').first()
        self.assertEqual(str(s3.title), "booktesting2")

        db.session.delete(s3)
        db.session.commit()
    
    def test_insert_book_3(self):
        s = Book(id='5003', title = 'booktest3')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Book).filter_by(id='5003').first()
        self.assertEqual(str(s2.id), '5003')

        s2.title = "booktesting3"
        db.session.commit()

        s3 = db.session.query(Book).filter_by(id='5003').first()
        self.assertEqual(str(s3.title), "booktesting3")

        db.session.delete(s3)
        db.session.commit()
    
    def test_insert_author_1(self):
        s = Author(id='5004', name = 'authortest1')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Author).filter_by(id='5004').first()
        self.assertEqual(str(s2.id), '5004')

        s2.name = "authortesting1"
        db.session.commit()

        s3 = db.session.query(Author).filter_by(id='5004').first()
        self.assertEqual(str(s3.name), "authortesting1")

        db.session.delete(s3)
        db.session.commit()
    
    def test_insert_author_2(self):
        s = Author(id='5005', name = 'authortest2')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Author).filter_by(id='5005').first()
        self.assertEqual(str(s2.id), '5005')

        s2.name = "authortesting2"
        db.session.commit()

        s3 = db.session.query(Author).filter_by(id='5005').first()
        self.assertEqual(str(s3.name), "authortesting2")

        db.session.delete(s3)
        db.session.commit()
    
    def test_insert_author_3(self):
        s = Author(id='5006', name = 'authortest3')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Author).filter_by(id='5006').first()
        self.assertEqual(str(s2.id), '5006')

        s2.name = "authortesting3"
        db.session.commit()

        s3 = db.session.query(Author).filter_by(id='5006').first()
        self.assertEqual(str(s3.name), "authortesting3")

        db.session.delete(s3)
        db.session.commit()
    
    def test_insert_publisher_1(self):
        s = Publisher(id='5007', name = 'publishertest1')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Publisher).filter_by(id='5007').first()
        self.assertEqual(str(s2.id), '5007')

        s2.name = "publishertesting1"
        db.session.commit()

        s3 = db.session.query(Publisher).filter_by(id='5007').first()
        self.assertEqual(str(s3.name), "publishertesting1")

        db.session.delete(s3)
        db.session.commit()
    
    def test_insert_publisher_2(self):
        s = Publisher(id='5008', name = 'publishertest2')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Publisher).filter_by(id='5008').first()
        self.assertEqual(str(s2.id), '5008')

        s2.name = "publishertesting2"
        db.session.commit()

        s3 = db.session.query(Publisher).filter_by(id='5008').first()
        self.assertEqual(str(s3.name), "publishertesting2")

        db.session.delete(s3)
        db.session.commit()
    
    def test_insert_publisher_3(self):
        s = Publisher(id='5009', name = 'publishertest3')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Publisher).filter_by(id='5009').first()
        self.assertEqual(str(s2.id), '5009')

        s2.name = "publishertesting3"
        db.session.commit()

        s3 = db.session.query(Publisher).filter_by(id='5009').first()
        self.assertEqual(str(s3.name), "publishertesting3")

        db.session.delete(s3)
        db.session.commit()

    def test_insert_publisher_4(self):
        s = Publisher(id='5010', name = 'publishertest4')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Publisher).filter_by(id='5010').first()
        self.assertEqual(str(s2.id), '5010')

        s2.name = "publishertesting4"
        db.session.commit()

        s3 = db.session.query(Publisher).filter_by(id='5010').first()
        self.assertEqual(str(s3.name), "publishertesting4")

        db.session.delete(s3)
        db.session.commit()

    def test_insert_publisher_5(self):
        s = Publisher(id='5011', name = 'publishertest5')
        db.session.add(s)
        db.session.commit()

        s2 = db.session.query(Publisher).filter_by(id='5011').first()
        self.assertEqual(str(s2.id), '5011')

        s2.name = "publishertesting5"
        db.session.commit()

        s3 = db.session.query(Publisher).filter_by(id='5011').first()
        self.assertEqual(str(s3.name), "publishertesting5")

        db.session.delete(s3)
        db.session.commit()

if __name__ == '__main__':
	unittest.main()

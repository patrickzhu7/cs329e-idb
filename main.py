#-----------------------------------------
# main.py
# creating first flask application
#-----------------------------------------



from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_paginate import Pagination, get_page_parameter
import json
from models import app, db, Book, Author, Publisher
import test
import unittest
import subprocess
from StringIO import StringIO


#book_data = json.load(open('books.json'))


# create a flask object (flask needs an object to represent the application)

#app = Flask(__name__)
# we move this to models.py


# The route decorator '@app.route()' maps a function to a route on your website.
# decorators are used to map a function, index(), to a web page, / or
# i.e., when someone types in the home address of the web site,
# flask will run the function index()
# summary: type in a URL, flask check the URL, finds the associate function with it, runs the function, collect responses, and send back the results to the browser.

@app.route('/')
def index():
    return render_template('hello.html')

@app.route('/about/')
def about():
	#suite = unittest.TestLoader().loadTestsFromModule(test)
	suite = unittest.TestLoader().discover("test")
	string_store = StringIO("Test cases")
	
	unittest.TextTestRunner(verbosity=2, stream=string_store).run(suite)

	results = str(string_store.getvalue())
	results = results.split("\n")

	return render_template('about.html', results = results)

@app.route('/books/')
def books():
    books = db.session.query(Book).all()
    return render_template('books.html', books = books)

@app.route('/books2/')
def books2():
    books = db.session.query(Book).all()
    return render_template('books2.html', books = books)

@app.route('/books3/')
def books3():
    books = db.session.query(Book).all()
    return render_template('books3.html', books = books)

@app.route('/books/<int:book_id>')
def singlebook(book_id):
	single_book = db.session.query(Book).get(book_id)
	return render_template('singlebook.html', book_id = book_id, book = single_book)

@app.route('/authors/')
def authors():
    authors = db.session.query(Author).all()
    return render_template('authors.html', authors = authors)

@app.route('/authors2/')
def authors2():
    authors = db.session.query(Author).all()
    return render_template('authors2.html', authors = authors)

@app.route('/authors3/')
def authors3():
    authors = db.session.query(Author).all()
    return render_template('authors3.html', authors = authors)

@app.route('/authors/<int:author_id>')
def singleauthor(author_id):
	single_author = db.session.query(Author).get(author_id)
	books = db.session.query(Book).all()
	return render_template('singleauthor.html', author_id = author_id, author = single_author,books=books)

@app.route('/publishers/')
def publishers():
    publishers = db.session.query(Publisher).all()
    return render_template('publishers.html', publishers = publishers)

@app.route('/publishers2/')
def publishers2():
    publishers = db.session.query(Publisher).all()
    return render_template('publishers2.html', publishers = publishers)

@app.route('/publishers3/')
def publishers3():
    publishers = db.session.query(Publisher).all()
    return render_template('publishers3.html', publishers = publishers)

@app.route('/publishers/<int:pub_id>')
def singlepublisher(pub_id):
	single_publisher = db.session.query(Publisher).get(pub_id)
	books = db.session.query(Book).all()
	return render_template('singlepublisher.html', pub_id = pub_id, publisher = single_publisher,books=books)

@app.route('/test/')
def test():
	suite = unittest.TestLoader().discover("test")
	string_store = StringIO("Test cases")
	
	unittest.TextTestRunner(verbosity=2, stream=string_store).run(suite)

	results = str(string_store.getvalue())
	results = results.split("\n")

	return render_template('test.html', results = results)

	#p = subprocess.Popen(["python", "test.py"],
			#stdout=subprocess.PIPE,
			#stderr=subprocess.PIPE,
			#stdin=subprocess.PIPE)
	#out, err = p.communicate()
	#output=err+out
	#output = output.decode("utf-8") #convert from byte type to string type
	
	#return render_template('test.html', output = output)

# if main.py is run directly, i.e., as the main module, it will be assigned the value main
# and if it's main go ahead and run the application.
# if this application is imported, then the __name__ is no longer __main__ and
# the code, app.run(), will not be executed

if __name__ == "__main__":
    app.debug = True

    #leave this line so lydia can run the website on her local machine thanks
    #app.run('localhost', '80')

    app.run('104.248.122.33', '80')

#----------------------------------------
# end of main.py
#----------------------------------------
